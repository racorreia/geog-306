# GEOG-306 Quantitative methods of data analysis in geography

Welcome to the repository for the course "Quantitative methods of data analysis in geography" (GEOG-306), part of the Master's Programme in Geography at the University of Helsinki.

## Learning outcomes
The goal of this introiductory course is to provide students with the foundations of quantitative analysis methods, including data exploration, visualization and statistical analysis, as part of MSc and PhD education in Geography. After completing the course, the participants will be able to:

- Implement data analysis and visualization methods using R statistical software 
- Identify and select among suitable statistical methods for data analysis
- Apply the most appropriate methods for their research data
- Write the results of statitical analyses in a suitable manner for theses, articles, and reports

## Contexts

This repository contains the data and scripts used in the practical exercises that are carried out during the lectures.

## Authors
The data and code available here were developed by Ricardo Correia, in collaboration with Enrico Di Minin who leads the course.

## Latest update
Last updated in February 2022

## Upcoming updates
There are plans to convert the course into an R package using learnr, stay tuned for more details.

