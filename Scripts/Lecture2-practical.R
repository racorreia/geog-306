# Clean your R environment first
# By the way, you have noticed by now that lines in your script starting with the cardinal sign '#' are not read by R
# This is the best way to annotate your code in a way that can help others follow it, or to take notes during class
rm(list=ls())

# We'll start by exploring data types
# Let's read the data in dataset nb_crime.csv
mydata <- read.csv("./Data/nb_crime.csv")

# Check the structure of the data
str(mydata)
# How many data types do you identify in the dataset?
# You can check this by looking at the three character string after the variable name
# E.g. neighborhood : *chr*


# Let's start by looking at continuous data types
# There are two types of continuous data recognized by R - Numerical and Integer
# Let's create a random variable composed of numbers selected at random between 0 and 100
var1 <- runif(5, min=0, max=100)
# How does the variable look like?
var1

# Now let's test what type of continuous variable it is
# You can do this with is.numeric and is.integer
# Let's try them out
is.numeric(var1)
is.integer(var1)

# We can see that the variable we created is considered a numeric variable
# However, continuous variables can change type if needed
# But what happens if we convert the variable to integer?
var2 <- as.integer(var1)
is.integer(var2)
# It is now an integer, but how does it look like?
var2
# Let's compare it with the original variable
var1
# As you can see, integer variables can only hold whole numbers, not decimal or fractional ones
# If you are working with decimal data, make sure it is coded as numeric or you risk losing information
# This is important to keep in mind for some applications, but for most statistical tests it is not a problem as R recognizes integers as a special form of numeric variables
is.numeric(var2)


# Ok, let's explore categorical variables now
# Lets go back to our dataset, can you identify any categorical variables?
str(mydata)
# We have no variables coded as categorical but we have two variables coded as text: neighborhood and crime_frequency
# Let's check them out
mydata$neighborhood
# We can variable neighborhood refers to the identify of different neighborhoods Alfama, Benfica, Carmo and Desvio
mydata$crime_frequency
# In contrast, variable crime_frequency corresponds to a variable that ranges from Very low to Very high

# Both these variables can be considered categorical, but R doesn't recognize them as such
# Let's convert variable neighborhood to a categorical variable
# The standard class for categorical variables in R are called *factors*
as.factor(mydata$neighborhood)
# As you can see, when you call variable neighborhood as factor, R recognizes it as a categorical variables with 4 levels ranging from A to D
# Let's check our dataset structure again
str(mydata)
# Do you notice any difference? And if not, can you figure out why not?

# If you cannot see any difference, you are correct :)
# This happens because in the previous line we called variable neighborhood as a factor, but we didn't save it in an object
# Let's confirm this by specifically testing whether R considers the variable as factor
is.factor(mydata$neighborhood)
# Let's change that by overwriting the variable in the dataset
mydata$neighborhood <- as.factor(mydata$neighborhood)
# Let's check the data again
str(mydata)
is.factor(mydata$neighborhood)
# Great, now R recognizes the first column in the data as a factorial variable with four levels or categories
# We can specifically check the levels of a categorical variable by calling function levels
levels(mydata$neighborhood)
# However, you may have noticed something else - That R organized the levels alphabetically from Alfama to Desvio
# When we print the variable, the first level in the data is Benfica but in levels R recognizes Alfama as the first level
mydata$neighborhood
# We can double-check this by matching the data with the levels of the variable as seen below
match(mydata$neighborhood, levels(mydata$neighborhood))
# We can see that the first entry in our data matches the second level of the variable
# Sometimes, we may want to keep the levels in the original order or assign them a different one (e.g. for plotting)
# We can do this using function factor rather than using as factor
# Let's try to assign the levels as they appear in the data
mydata$neighborhood <- factor(mydata$neighborhood,
                              levels = c("Benfica", "Alfama", "Carmo", "Desvio"))
levels(mydata$neighborhood)
mydata$neighborhood

# In the last lecture, we also saw one way to change this directly when reading the data. Try to run that again now (if you can't remember, check ?read.csv for help).
# What is the default behavior with regards to factor levels when reading text data as factors?

# Ok, now let's focus on the other categorical variable in the data pertaining to crime frequency
# Let's check it out
mydata$crime_frequency
# Ok, let's convert the variable to factorial
mydata$crime_frequency <- as.factor(mydata$crime_frequency)
is.factor(mydata$crime_frequency)
mydata$crime_frequency
# How is the data organized? Do you notice something particular about these data?

# The data are composed of categories that have an order to them
# Very high > High > Medium > Low > Very low
# However, as before R has organized the levels by alphabetical order
# For example, let's see what happens when we draw a boxplot of unmeployment_rate in relation to crime_frequency
boxplot(unemployment_rate~crime_frequency, mydata)
# R plots the factors in the same alphabetical order
# Let's repeat the process above an reorganize them in the relevant order and draw the boxplot again

# Now the figure makes more sense! However, while R recognizes the new order, it doesn't recognize the variable as having an order per se
is.ordered(mydata$crime_frequency)
# Is there a way for R to recognize this categorical variable as ordered? let's check the help file for ?factor

# Indeed, the argument ordered or function ordered can tell R that our factors have a relevant order to them
# Let's try that out
mydata$crime_frequency <- factor(mydata$crime_frequency,
                                 ordered = T)
is.ordered(mydata$crime_frequency)
# It seems to have worked! Let's check the variable
mydata$crime_frequency
# Oops! R recognizes the levels are ordered, but the order of the levels is wrong. Can you tell why?

# Indeed, because we didn't specify the order, R again assumed the order was alphabetical
# Also, R by default assumes the first level is the lowest in the order
# Let's correct this
mydata$crime_frequency <- factor(mydata$crime_frequency,
                                 levels = c("Very low", "Low", "Medium", "High", "Very high"),
                                 ordered = T)
mydata$crime_frequency
# Ok, now it worked! Let's try and plot the data again
boxplot(unemployment_rate~crime_frequency, data = mydata)

# Let's try a final example and see how the uemployment_rate has changed over the years across neighborhoods
# Let's do a plot of these variables, choosing unemployment_rate as the dependent variable and year as the independent variable
plot(unemployment_rate~as.factor(year), data = mydata)

# What can you tell about this plot? Is it easy to interpret?
# Try to run the same function but change year to a categorical variable.
# What changes? And do you see a pattern? Let's discuss these after the exercise!



########################
# End of first practical
########################

# Let's clean our R environment again
rm(list=ls())

# Let's read the data in the file nb_crime.csv again
mydata <- read.csv("./Data/nb_crime.csv",stringsAsFactors = T)
# And check the structure of the data
str(mydata)

# We will now focus on checking the distribution of the data and testing the assumptions needed for statistical tests
# Let's focus on the two continuous variables we have about the neighborhoods - unemployment_rate and arrests
# Let's plot first the data on unemployment rate
hist(mydata$unemployment_rate,
     main="Histogram of unemployment rate")
# Sometimes it helps to look at the data differently
# Now there are 5 beaks in the data, let's try to run the same plot with 10 breaks in the data
hist(mydata$unemployment_rate,
     breaks = 10,
     main="Histogram of unemployment rate")
# How does the data look like? It seems heavily skewed with many values in the interval between 0 to 2
# Let's plot the same histogram with the density of entries in in interval and plot the density curve of the data 
hist(mydata$unemployment_rate,
     probability = T,
     main="Histogram of unemployment rate")
lines(density(mydata$unemployment_rate),
      col = "red")

# Let's repeat the plot for the data on arrests below.
# What does the plot for arrests look like? Does it differ much from the previous one?
# And do you think any of these variables is normally distributed based on visual inspection?


# Now that you have plotted the data on arrests as well, let's look at the data using a different type of plot
# The normal Q-Q plot is an alternative graphical method of assessing normality to the histogram
# It is easier to use when there are small sample sizes.
# This type of scatter plot compares the data to a perfect normal distribution.
qqnorm(mydata$unemployment_rate,
       main = "QQ plot",
       pch=19)
qqline(mydata$unemployment_rate)
# If the data are normal, they tend to follow a liner patterns and be aligned along the line
# Let's try this type of plot now with the data on arrests below


# How does this plot look like? Have your predictions changed based when compared to looking at the data from the histogram?
# Let's test your predictions more formally using a normality test
# The Shapiro-Wilk tests the data against a null hypothesis that it is normally distributed
# If the test's p-value > 0.05, the null hypothesis is not rejected and thus normality can be assumed.
# Let's do the test for the unemployment_rate variable, you can check details on the test using ?shapiro.test
shapiro.test(mydata$unemployment_rate)
# What was the result? Did it match your expectations? Try the test also for the arrests data below and discuss the result
shapiro.test(mydata$arrests)

# Now let's check the assumption of homogeneity of variance between groups
# We will check if the variance in unemployment_rate and arrests is similar between neighborhoods over the years
# Let's start to explore this visually, first for unemployment_rate data and then for arrests
plot(unemployment_rate~neighborhood, data = mydata)
plot(arrests~neighborhood, data = mydata)
# Do you think the range of values is more or less equally distributed between neighborhoods for the two variables?
# Or are there neighborhoods for which there was more variance over the years than others?

# As before, let's test your predictions with a statistical test
# We can use the Bartlett test for this, check ?bartlett.test for more details on how it is implemented
bartlett.test(unemployment_rate~neighborhood, data = mydata)
# Are the results for unemployment_rate what you had expected? Try the test also for the arrests data see if it matches your expectations
bartlett.test(arrests~neighborhood, data = mydata)

# There is another test for homogeneity of variance that is more appropriate for non-normal data
# This test is called Levene test, but it is not available in the base R package
# To access it, you need to install and load a specific library in R called 'car'
# To install this library you can run the function blow
install.packages("car")
# You can install any other relevant library using the same function but changing the package name
# Alternatively, you can access the tab 'Packages' on the right, click the install button and locate the package you seek on the CRAN repository
# Now that thew package is installed, let's load it
library(car)
# Note that with function library you don't need to use quotes for the package name, unlike with function install.packages()
# Let's run Levene test on the unemployment_rate data as we saw before that the data does not follow the normal distribution
leveneTest(unemployment_rate~neighborhood, data = mydata)
# Did the results change when compared to those obtained with Bartlett test?

# Finally, let's have a go at testing the relationship between these two variables - unemployment_rate and arrests
# Do you expect any relationship between them? If so, what is the nature of that relationship? Positive or negative?


# Again, let's confirm your predictions
# Because we are dealing with two continuous variables, and one of them is not normally distributed, we can use Spearman's correlation test
# There are several ways to calculate this test in R, but let's use the one available in package 'pspearman'
# For this, we need to install the package first and then load it
install.packages("pspearman")
library(pspearman)
# Ok, now we can run the test
spearman.test(x = mydata$arrests,
              y = mydata$unemployment_rate)
# Is there a significant relationship? Is it positive or negative? Does this match your expectation?

# What could be the driver of this relationship? Could it be that neighborhoods with higher unemployment have higher crime rates and therefore more arrests?
# Let's test this hypothesis by assessing the relationship between unemployment and crime frequency
# Spearman tests can handle ordered categorical data so we can use the same function for the test
spearman.test(x = mydata$unemployment_rate,
              y = mydata$crime_frequency)

# What was the test result? Did you see a significant positive relationship as expected?
# If not, let's check the data
str(mydata)
# Is crime_frequency recognized as an ordered variable by R? And is the ordering of factors correct?
# If not, use the example code from the first session and concert the variable again to ordered factors and repeat the analysis
# Did anything change?


# Ok, let's think how we would explore the relationship for other variables
# Which tests would you use to compare
# - Crime frequency between neighborhoods
# - Arrests between neighborhoods
# - Unemployment rates over the years


